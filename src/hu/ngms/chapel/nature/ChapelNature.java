package hu.ngms.chapel.nature;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;

public final class ChapelNature implements IProjectNature{
    
    private static final String NATURE_ID = "hu.ngms.chapel.projectNature";
    private IProject project = null;
    
    public ChapelNature() {
	// TODO Auto-generated constructor stub
    }

    @Override
    public void configure() throws CoreException {
	// TODO Auto-generated method stub
	
    }

    @Override
    public void deconfigure() throws CoreException {
	// TODO Auto-generated method stub
	
    }

    @Override
    public IProject getProject() {
	// TODO Auto-generated method stub
	return this.project;
    }

    @Override
    public void setProject(IProject project) {
	this.project = project;
    }
    
    public static String getNatureId() {
	return NATURE_ID;
    }

}
